package com.gz.oauth.server.service;

import com.gz.oauth.server.pojo.entity.SysUserEntity;

public interface SysUserService {
    /**
     * 根据用户名查询用户信息
     * @param username
     * @return
     */
    SysUserEntity selectByUsername(String username);

}
