package com.gz.oauth.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gz.oauth.server.pojo.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysUserMapper extends BaseMapper<SysUserEntity> {

}
