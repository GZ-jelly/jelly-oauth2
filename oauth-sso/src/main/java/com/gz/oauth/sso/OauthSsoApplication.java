package com.gz.oauth.sso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OauthSsoApplication {

    public static void main(String[] args) {
        SpringApplication.run(OauthSsoApplication.class, args);
    }

}
