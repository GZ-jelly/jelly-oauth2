# jelly-oauth2

#### 介绍
基于Spring Authorization Server 开发,实现了oauth2授权服务器、oauth2客户端、oauth2资源服务器

#### 开发环境
* jdk17
* Spring Authorization Server0.4.3
* maven 3.89
* springboot 2.7.14

